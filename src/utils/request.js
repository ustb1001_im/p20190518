import axios from 'axios'
import { doer } from '@/utils/request-interceptor'
// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})
doer(service)

export default service
