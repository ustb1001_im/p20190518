import Vue from 'vue'
import Router from 'vue-router'
/* Layout */
import Layout from '@/layout'

Vue.use(Router)

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [{
        path: '/login',
        component: () =>
            import ('@/views/login/index'),
        hidden: true
    },

    {
        path: '/404',
        component: () =>
            import ('@/views/404'),
        hidden: true
    },

    {
        path: '/',
        name: 'Index',
        hidden: true,
        redirect: '/dashboard'
    },
    {
        path: '/dashboard',
        component: Layout,
        redirect: '/dashboard/overview',
        name: 'Dashboard',
        meta: {
            title: 'dashboard',
            icon: 'dashboard'
        },
        children: [{
            path: 'overview',
            name: 'Overview',
            component: () =>
                import ('@/views/dashboard/index'),
            meta: { title: 'overview' }
        }, {
            path: 'dataset',
            name: 'Dataset',
            component: () =>
                import ('@/views/dashboard/dataset'),
            meta: { title: 'dataset' }
        }, {
            path: 'paste-preparation-system',
            name: 'paste-preparation-system',
            component: () =>
                import ('@/views/dashboard/paste-preparation-system/index'),
            meta: { title: 'paste-preparation-system' },
            hidden: true
        }]
    },
    {
        path: '/stope',
        component: Layout,
        redirect: '/stope/manage',
        name: 'stope',
        children: [{
                path: 'manage',
                name: 'stopeManage',
                component: () =>
                    import ('@/views/stope/index'),
                meta: { title: 'stope', icon: 'stope' }
            },
            {
                path: ':id',
                name: 'stopeDetails',
                component: () =>
                    import ('@/views/stope/details/index'),
                hidden: true
            }
        ]
    },
    {
        path: '/log',
        component: Layout,
        name: 'log',
        meta: {
            title: 'log',
            icon: 'documentation'
        },
        children: [{
                path: 'history-data',
                name: 'HistoryData',
                component: () =>
                    import ('@/views/log/history-data'),
                meta: { title: 'historyData' }
            },
            {
                path: 'recommend',
                name: 'Recommend',
                component: () =>
                    import ('@/views/log/recommend'),
                meta: { title: 'recommend' }
            },
            {
                path: 'warning',
                name: 'Warning',
                component: () =>
                    import ('@/views/log/warning'),
                meta: { title: 'warning' }
            }
        ]
    },
    {
        path: '/inst',
        component: Layout,
        children: [{
                path: 'manage',
                name: 'instManage',
                component: () =>
                    import ('@/views/inst/index'),
                meta: { title: 'inst', icon: 'instrument' }
            },
            {
                path: 'detail/:id',
                name: 'instDetail',
                component: () =>
                    import ('@/views/inst/detail'),
                // meta: { title: 'instDetail', noCache: true, activeMenu: '/inst/manage' },
                hidden: true
            }
        ]
    },
    // 404 page must be placed at the end !!!
    { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
    mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
    const newRouter = createRouter()
    router.matcher = newRouter.matcher // reset router
}

export default router