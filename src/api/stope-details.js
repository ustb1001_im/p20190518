import request from '@/utils/request1'

export function getData(type, params) {
    return request({
        url: '/stope-details/' + type,
        method: 'get',
        params
    })
}

export function updateDetail(data) {
    return request({
        url: '/stope-details/main/update',
        method: 'post',
        data
    })
}