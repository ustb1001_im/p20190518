import request from '@/utils/request1'

export function getChartData(type, id, params) {
  return request({
    url: '/paste/' + type + '/chart/all?id=' + id,
    method: 'get',
    params
  })
}

export function updateChartData(type, id, data) {
  return request({
    url: '/paste/' + type + '/chart/update?id=' + id,
    method: 'post',
    data
  })
}

export function updateParas(type, id, data) {
  return request({
    url: '/paste/' + type + '/paras/update?id=' + id,
    method: 'get',
    data
  })
}
