import request from '@/utils/request1'

export function getList(data) {
    return request({
        url: '/inst/inst_list',
        method: 'post',
        data
    })
}
export function getDetail(params) {
    return request({
        url: '/inst/detail/all',
        method: 'get',
        params
    })
}
export function updateDetail(data) {
    return request({
        url: '/inst/detail/update',
        method: 'post',
        data
    })
}
export function updateTable(data) {
    return request({
        url: '/inst/detail/updatTable',
        method: 'post',
        data
    })
}
export function updateDetailChart(data) {
    return request({
        url: '/inst/detail/updateChart',
        method: 'post',
        data
    })
}

export function updateDetailTable(data) {
    return request({
        url: '/inst/detail/updateTable',
        method: 'post',
        data
    })
}