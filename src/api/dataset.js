import request from '@/utils/request1'

export function getData(params) {
    return request({
        url: '/dataset1/all',
        method: 'get',
        params
    })
}

export function updateData(data) {
    return request({
        url: '/dataset1/update',
        method: 'post',
        data
    })
}

export function getData2(params) {
    return request({
        url: '/dataset2/all',
        method: 'get',
        params
    })
}

export function updateData2(data) {
    return request({
        url: '/dataset2/update',
        method: 'post',
        data
    })
}