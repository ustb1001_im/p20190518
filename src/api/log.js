import request from '@/utils/request1'

export function getLog(type, data) {
  return request({
    url: '/log/list/' + type,
    method: 'post',
    data
  })
}
