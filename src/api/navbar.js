import request from '@/utils/request'

export function getEmails(params) {
  return request({
    url: '/emails/list',
    method: 'get',
    params
  })
}
