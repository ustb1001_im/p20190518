import request from '@/utils/request1'

export function getList(data) {
    return request({
        url: '/stope/stope_list',
        method: 'post',
        data
    })
}