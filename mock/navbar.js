import Mock from 'mockjs'

const data = Mock.mock({
  'items|6': [{
    id: '@id',
    time: '@datetime',
    title: '@sentence(10, 20)',
    content: '@sentence(10, 100)'
  }]
})

export default [
  {
    url: '/emails/list',
    type: 'get',
    response: config => {
      const items = data.items
      return {
        code: 20000,
        data: items
      }
    }
  }
]
