const token = 'admin-token'
export default [
    // user login
    {
        url: '/user/login',
        type: 'post',
        response: config => {
            const { username } = config.body
                // mock error
            if (username !== 'admin') {
                return {
                    code: 60204,
                    message: 'Account and password are incorrect.'
                }
            }

            return {
                code: 20000,
                data: { token: token }
            }
        }
    },

    // get user info
    {
        url: '/user/info\.*',
        type: 'get',
        response: config => {
            const info = {
                    name: 'admin',
                    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
                    stopeList: [{ id: 1222, name: '测试采场1' }, { id: 122, name: '测试采场2' }],
                    settings: {
                        inst: [
                            { id: 122, name: '填充系统1', choice: 3, stope: 1222 },
                            { id: 221, name: '填充系统2', choice: 3, stope: 1222 }
                        ]
                    }

                }
                // mock error
            if (!info) {
                return {
                    code: 50008,
                    message: 'Login failed, unable to get user details.'
                }
            }

            return {
                code: 20000,
                data: info
            }
        }
    },

    // user logout
    {
        url: '/user/logout',
        type: 'post',
        response: _ => {
            return {
                code: 20000,
                data: 'success'
            }
        }
    }
]