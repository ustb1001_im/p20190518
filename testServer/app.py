#!/usr/bin/env python3
# coding:utf-8
import random
from flask import Flask, jsonify, request
from flask_cors import CORS
from typing import Any
from flask_socketio import SocketIO, emit, disconnect
import threading
from time import sleep
import json
from threading import Lock
import json
import datetime
import iso8601
import time

app = Flask(__name__)
CORS(app, supports_credentials=True)


def similar(a, b):
    if 'all' in [a, b]:
        return True

    return str(a) in str(b) or str(b) in str(a) or (a == '是' and b == 'true') or (a == '否' and b == 'false')


def sa(d):
    for _ in d:
        if not similar(d[_], getPostData(_, '')):
            return False
    return True


def getPostData(key, default):
    return request.get_json().get(key, default)


def jresponse(arg):
    _ = jsonify(
        {'code': 20000, 'data': {
            'total': arg[0],
            'items': arg[1]
        }}
    )
    return _


def jresponse1(arg):
    _ = jsonify(
        {'code': 20000, 'data': arg}
    )
    return _


def myFilter(l):
    limit = int(getPostData('limit', '0'))
    page = int(getPostData('page', 1))
    l = filter(sa, l)

    if getPostData('date', None):
        def _(data):
            d = getPostData('date', None)
            d1, d2 = [iso8601.parse_date(i) for i in d]
            if d1 <= datetime.datetime.now(d1.tzinfo) <= d2:
                return True

        l = filter(_, l)
    l = list(l)
    if limit < 1:
        return l
    a = (page - 1) * limit
    b = page * limit
    return len(l), l[a:b]


@app.route('/random')
def rd():
    if request.method == 'POST':
        l = request.form.keys()
    else:
        l = request.args.keys()
    return jresponse1({k: random.randint(1, 100) for k in l})


socketio = SocketIO(app)
thread = None
thread_lock = Lock()


def gen(m) -> dict:
    return {k: random.randint(1, 100) for k in m}


def background_thread():
    """Example of how to send server generated events to clients."""
    count = 0
    while True:
        socketio.sleep(1)
        count += 1
        data = gen(['bottom_concentration', 'downhole_pipeline',
                    'stope', 'filling_progress'])
        data.update({'working_mode': 'filling'})
        socketio.emit('realtime', data, broadcast=True)

        data = gen(['h1', 'h2', 'h3', 'h4', 'h5',
                    'h6', 'h7', 'h8', 'h9', 'h10'])
        data.update({'m1': 'tailing', 'm2': 'normal'})
        socketio.emit('index', data, broadcast=True)


@socketio.on('disconnect_request')
def disconnect_request():
    emit('my_response',
         {'data': 'Disconnected!'})
    disconnect()


@socketio.on('my_ping')
def ping_pong():
    emit('my_pong')


@socketio.on('connect')
def test_connect():
    global thread
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(background_thread)
    emit('my_response', {'data': 'Connected', 'count': 0})


@app.route('/inst/inst_list', methods=['POST'])
def ins():
    l = [{"status": "正常", "instrument": "浊度计", "serial": "AT-311TH03"},
         {"status": "异常", "instrument": "浊度计", "serial": "AT-311TH07"},
         {"status": "异常", "instrument": "浊度计", "serial": "AT-311TH08"},
         {"status": "正常", "instrument": "浊度计", "serial": "AT-311TH07"},
         {"status": "异常", "instrument": "浊度计", "serial": "AT-311TH01"},
         {"status": "异常", "instrument": "浊度计", "serial": "AT-311TH01"},
         {"status": "异常", "instrument": "浊度计", "serial": "AT-311TH06"},
         {"status": "异常", "instrument": "浊度计", "serial": "AT-311TH04"},
         {"status": "正常", "instrument": "浊度计", "serial": "AT-311TH04"},
         {"status": "异常", "instrument": "浊度计", "serial": "AT-311TH01"},
         {"status": "正常", "instrument": "浊度计", "serial": "AT-311TH07"},
         {"status": "异常", "instrument": "浊度计", "serial": "AT-311TH04"},
         {"status": "异常", "instrument": "浊度计", "serial": "AT-311TH07"},
         {"status": "正常", "instrument": "浊度计", "serial": "AT-311TH02"},
         {"status": "正常", "instrument": "浊度计", "serial": "AT-311TH06"},
         {"status": "异常", "instrument": "浊度计", "serial": "AT-311TH07"},
         {"status": "异常", "instrument": "浊度计", "serial": "AT-311TH06"},
         {"status": "正常", "instrument": "浊度计", "serial": "AT-311TH04"},
         {"status": "异常", "instrument": "浊度计", "serial": "AT-311TH09"},
         {"status": "正常", "instrument": "浊度计", "serial": "AT-311TH05"},
         {"status": "异常", "instrument": "浊度计", "serial": "AT-311TH06"},
         {"status": "正常", "instrument": "浊度计", "serial": "AT-311TH02"},
         {"status": "异常", "instrument": "浊度计", "serial": "AT-311TH02"},
         {"status": "异常", "instrument": "浊度计", "serial": "AT-311TH07"},
         {"status": "异常", "instrument": "浊度计", "serial": "AT-311TH02"},
         {"status": "正常", "instrument": "浊度计", "serial": "AT-311TH01"},
         {"status": "正常", "instrument": "浊度计", "serial": "AT-311TH01"},
         {"status": "异常", "instrument": "浊度计", "serial": "AT-311TH06"},
         {"status": "正常", "instrument": "浊度计", "serial": "AT-311TH03"},
         {"status": "正常", "instrument": "浊度计", "serial": "AT-311TH05"}]
    return jresponse(myFilter(l))


@app.route('/log/list/warning', methods=['POST'])
def log_warning():
    l = [{"id": "210000199008217424", "time": "1990-07-24 03:17:29", "instrument": "泵", "principal": "admin9",
          "Normal_working_range": "30-60", "Monitoring_value": 197},
         {"id": "320000200010181804", "time": "1982-04-14 23:08:12", "instrument": "深锥浓密机", "principal": "admin3",
          "Normal_working_range": "0-360", "Monitoring_value": 120},
         {"id": "650000201101133614", "time": "1993-10-30 11:47:16", "instrument": "深锥浓密机", "principal": "admin6",
          "Normal_working_range": "30-60", "Monitoring_value": 237},
         {"id": "310000199601128741", "time": "1987-01-01 14:50:02", "instrument": "深锥浓密机", "principal": "admin6",
          "Normal_working_range": "100-240", "Monitoring_value": 284},
         {"id": "51000019710124164X", "time": "1976-03-11 12:44:00", "instrument": "泵", "principal": "admin7",
          "Normal_working_range": "0-360", "Monitoring_value": 357},
         {"id": "140000197009146106", "time": "2017-03-11 07:22:12", "instrument": "深锥浓密机", "principal": "admin9",
          "Normal_working_range": "30-60", "Monitoring_value": 387},
         {"id": "650000200811216858", "time": "1987-04-13 09:24:34", "instrument": "深锥浓密机", "principal": "admin8",
          "Normal_working_range": "100-240", "Monitoring_value": 95},
         {"id": "54000019790709463X", "time": "2004-02-22 19:25:34", "instrument": "深锥浓密机", "principal": "admin3",
          "Normal_working_range": "0-360", "Monitoring_value": 94},
         {"id": "13000020100925768X", "time": "2012-06-26 17:54:13", "instrument": "泵", "principal": "admin6",
          "Normal_working_range": "0-360", "Monitoring_value": 14},
         {"id": "630000201609156813", "time": "1983-08-19 12:48:49", "instrument": "泵", "principal": "admin5",
          "Normal_working_range": "0-360", "Monitoring_value": 358},
         {"id": "420000197108299615", "time": "1997-02-14 19:49:47", "instrument": "泵", "principal": "admin1",
          "Normal_working_range": "30-60", "Monitoring_value": 106},
         {"id": "370000199604210936", "time": "1976-07-23 11:36:13", "instrument": "深锥浓密机", "principal": "admin1",
          "Normal_working_range": "0-360", "Monitoring_value": 276},
         {"id": "820000198801141137", "time": "1996-09-16 12:05:27", "instrument": "泵", "principal": "admin9",
          "Normal_working_range": "100-240", "Monitoring_value": 155},
         {"id": "310000201510148384", "time": "1990-05-23 16:04:04", "instrument": "深锥浓密机", "principal": "admin10",
          "Normal_working_range": "30-60", "Monitoring_value": 216},
         {"id": "220000200706208725", "time": "2013-07-07 23:51:57", "instrument": "深锥浓密机", "principal": "admin5",
          "Normal_working_range": "30-60", "Monitoring_value": 217},
         {"id": "650000200910110477", "time": "1975-03-24 19:12:57", "instrument": "泵", "principal": "admin6",
          "Normal_working_range": "30-60", "Monitoring_value": 27},
         {"id": "530000197209285613", "time": "2011-08-12 10:41:50", "instrument": "深锥浓密机", "principal": "admin2",
          "Normal_working_range": "100-240", "Monitoring_value": 117},
         {"id": "630000200407262250", "time": "2006-03-20 23:58:53", "instrument": "泵", "principal": "admin5",
          "Normal_working_range": "30-60", "Monitoring_value": 340},
         {"id": "370000198802286355", "time": "1987-12-01 05:30:21", "instrument": "泵", "principal": "admin5",
          "Normal_working_range": "0-360", "Monitoring_value": 157},
         {"id": "210000199210318982", "time": "1985-06-27 22:25:42", "instrument": "深锥浓密机", "principal": "admin3",
          "Normal_working_range": "100-240", "Monitoring_value": 18},
         {"id": "350000201103232834", "time": "2000-07-10 23:58:08", "instrument": "深锥浓密机", "principal": "admin0",
          "Normal_working_range": "0-360", "Monitoring_value": 267},
         {"id": "320000198102227566", "time": "1978-06-05 03:49:41", "instrument": "深锥浓密机", "principal": "admin0",
          "Normal_working_range": "0-360", "Monitoring_value": 192},
         {"id": "43000020071118398X", "time": "1994-03-03 08:08:47", "instrument": "深锥浓密机", "principal": "admin6",
          "Normal_working_range": "100-240", "Monitoring_value": 87},
         {"id": "320000201109205380", "time": "1996-06-24 15:52:01", "instrument": "深锥浓密机", "principal": "admin4",
          "Normal_working_range": "100-240", "Monitoring_value": 35},
         {"id": "460000199503088144", "time": "1987-04-02 05:12:18", "instrument": "深锥浓密机", "principal": "admin5",
          "Normal_working_range": "30-60", "Monitoring_value": 150},
         {"id": "460000197404233124", "time": "2013-07-16 22:49:14", "instrument": "泵", "principal": "admin2",
          "Normal_working_range": "0-360", "Monitoring_value": 282},
         {"id": "330000198409050445", "time": "1977-12-09 04:21:17", "instrument": "深锥浓密机", "principal": "admin1",
          "Normal_working_range": "0-360", "Monitoring_value": 38},
         {"id": "320000199601237130", "time": "1971-06-05 20:05:59", "instrument": "泵", "principal": "admin6",
          "Normal_working_range": "30-60", "Monitoring_value": 231},
         {"id": "42000019700424023X", "time": "1980-05-02 09:03:20", "instrument": "深锥浓密机", "principal": "admin6",
          "Normal_working_range": "30-60", "Monitoring_value": 81},
         {"id": "230000199812051481", "time": "1981-06-19 07:31:34", "instrument": "泵", "principal": "admin0",
          "Normal_working_range": "0-360", "Monitoring_value": 337}]
    return jresponse(myFilter(l))


@app.route('/log/list/recommend', methods=['POST'])
def log_recommend():
    l = [{"id": "110000200405156858", "time": "2002-06-03 00:38:33", "principal": "admin8", "instrument": "浓度计95",
          "Original_parameter": 76, "Modified_parameter": 39, "adoption": "否"},
         {"id": "360000199704056129", "time": "1978-05-25 13:31:30", "principal": "admin7", "instrument": "浓度计14",
          "Original_parameter": 33, "Modified_parameter": 13, "adoption": "否"},
         {"id": "42000020060520268X", "time": "1994-02-05 04:45:50", "principal": "admin0", "instrument": "浓度计20",
          "Original_parameter": 67, "Modified_parameter": 79, "adoption": "否"},
         {"id": "540000201511201178", "time": "1987-10-09 11:42:52", "principal": "admin9", "instrument": "浓度计66",
          "Original_parameter": 10, "Modified_parameter": 61, "adoption": "否"},
         {"id": "120000197807160806", "time": "1977-01-15 19:06:16", "principal": "admin1", "instrument": "浓度计26",
          "Original_parameter": 99, "Modified_parameter": 79, "adoption": "否"},
         {"id": "420000200706081039", "time": "2004-11-19 15:56:29", "principal": "admin9", "instrument": "浓度计93",
          "Original_parameter": 1, "Modified_parameter": 92, "adoption": "否"},
         {"id": "340000199210103569", "time": "1985-09-29 05:35:54", "principal": "admin7", "instrument": "浓度计70",
          "Original_parameter": 45, "Modified_parameter": 36, "adoption": "是"},
         {"id": "640000199506125282", "time": "2012-03-23 03:12:56", "principal": "admin0", "instrument": "浓度计71",
          "Original_parameter": 68, "Modified_parameter": 23, "adoption": "是"},
         {"id": "650000199401287615", "time": "1973-04-22 19:10:57", "principal": "admin4", "instrument": "浓度计12",
          "Original_parameter": 36, "Modified_parameter": 2, "adoption": "是"},
         {"id": "210000201712144222", "time": "1993-03-09 00:07:00", "principal": "admin10", "instrument": "浓度计7",
          "Original_parameter": 80, "Modified_parameter": 10, "adoption": "否"},
         {"id": "210000200107158699", "time": "1987-09-20 11:21:14", "principal": "admin9", "instrument": "浓度计64",
          "Original_parameter": 94, "Modified_parameter": 77, "adoption": "否"},
         {"id": "530000199209053942", "time": "1980-10-05 16:09:20", "principal": "admin3", "instrument": "浓度计24",
          "Original_parameter": 11, "Modified_parameter": 20, "adoption": "否"},
         {"id": "430000198301075329", "time": "1987-09-17 20:39:58", "principal": "admin2", "instrument": "浓度计93",
          "Original_parameter": 47, "Modified_parameter": 54, "adoption": "否"},
         {"id": "810000199809188047", "time": "1993-08-12 10:33:16", "principal": "admin4", "instrument": "浓度计11",
          "Original_parameter": 36, "Modified_parameter": 71, "adoption": "否"},
         {"id": "320000201503077321", "time": "1996-06-01 14:29:39", "principal": "admin6", "instrument": "浓度计86",
          "Original_parameter": 35, "Modified_parameter": 5, "adoption": "否"},
         {"id": "540000200404164928", "time": "2012-05-25 01:20:25", "principal": "admin6", "instrument": "浓度计4",
          "Original_parameter": 93, "Modified_parameter": 30, "adoption": "否"},
         {"id": "150000197610304445", "time": "1975-06-24 04:32:23", "principal": "admin3", "instrument": "浓度计14",
          "Original_parameter": 71, "Modified_parameter": 29, "adoption": "是"},
         {"id": "620000199306133188", "time": "1993-02-09 03:29:10", "principal": "admin4", "instrument": "浓度计16",
          "Original_parameter": 63, "Modified_parameter": 75, "adoption": "是"},
         {"id": "140000199611148744", "time": "1989-01-17 11:56:11", "principal": "admin4", "instrument": "浓度计86",
          "Original_parameter": 63, "Modified_parameter": 41, "adoption": "是"},
         {"id": "540000198207094975", "time": "2010-05-03 16:22:09", "principal": "admin1", "instrument": "浓度计28",
          "Original_parameter": 33, "Modified_parameter": 8, "adoption": "是"},
         {"id": "120000197512317677", "time": "2010-10-23 14:07:03", "principal": "admin5", "instrument": "浓度计19",
          "Original_parameter": 7, "Modified_parameter": 78, "adoption": "否"},
         {"id": "120000197008213713", "time": "1998-08-31 01:39:36", "principal": "admin0", "instrument": "浓度计20",
          "Original_parameter": 82, "Modified_parameter": 94, "adoption": "是"},
         {"id": "320000201604161709", "time": "1987-07-08 12:40:46", "principal": "admin7", "instrument": "浓度计73",
          "Original_parameter": 82, "Modified_parameter": 79, "adoption": "是"},
         {"id": "120000201005298386", "time": "1993-01-07 19:39:47", "principal": "admin1", "instrument": "浓度计35",
          "Original_parameter": 91, "Modified_parameter": 99, "adoption": "否"},
         {"id": "610000199510243585", "time": "1994-02-25 13:49:00", "principal": "admin4", "instrument": "浓度计49",
          "Original_parameter": 50, "Modified_parameter": 65, "adoption": "否"},
         {"id": "440000200309064810", "time": "1974-11-18 18:54:44", "principal": "admin3", "instrument": "浓度计71",
          "Original_parameter": 66, "Modified_parameter": 24, "adoption": "是"},
         {"id": "430000200205124354", "time": "1994-01-17 19:26:59", "principal": "admin6", "instrument": "浓度计77",
          "Original_parameter": 28, "Modified_parameter": 87, "adoption": "否"},
         {"id": "370000197509185788", "time": "2004-02-01 18:23:26", "principal": "admin7", "instrument": "浓度计77",
          "Original_parameter": 60, "Modified_parameter": 3, "adoption": "否"},
         {"id": "820000201201216507", "time": "2001-11-18 20:44:53", "principal": "admin6", "instrument": "浓度计39",
          "Original_parameter": 4, "Modified_parameter": 80, "adoption": "是"},
         {"id": "370000200806303896", "time": "1993-05-05 03:17:55", "principal": "admin2", "instrument": "浓度计89",
          "Original_parameter": 85, "Modified_parameter": 62, "adoption": "否"}]
    return jresponse(myFilter(l))


@app.route('/log/list/history', methods=['POST'])
def log_history():
    l = [{"id": "640000199312214156", "time": "1977-04-26 03:41:48", "instrument": "浓度计20", "Monitoring_value": "86%",
          "alarm": "是"},
         {"id": "410000197608216490", "time": "1980-12-18 14:34:55", "instrument": "浓度计71", "Monitoring_value": "9%",
          "alarm": "是"},
         {"id": "710000199510033223", "time": "1983-07-28 22:41:11", "instrument": "浓度计75", "Monitoring_value": "31%",
          "alarm": "否"},
         {"id": "34000020051021694X", "time": "1978-01-08 22:19:07", "instrument": "浓度计44", "Monitoring_value": "39%",
          "alarm": "否"},
         {"id": "460000197908204175", "time": "1975-06-10 16:08:19", "instrument": "浓度计22", "Monitoring_value": "58%",
          "alarm": "是"},
         {"id": "130000201502227564", "time": "1991-02-01 22:35:13", "instrument": "浓度计63", "Monitoring_value": "24%",
          "alarm": "否"},
         {"id": "540000198201146762", "time": "1971-07-12 16:16:25", "instrument": "浓度计10", "Monitoring_value": "16%",
          "alarm": "是"},
         {"id": "310000201310077227", "time": "2006-01-06 00:42:54", "instrument": "浓度计11", "Monitoring_value": "13%",
          "alarm": "是"},
         {"id": "500000197707062740", "time": "1971-03-04 03:14:48", "instrument": "浓度计67", "Monitoring_value": "38%",
          "alarm": "否"},
         {"id": "500000201010036429", "time": "1999-05-23 15:54:47", "instrument": "浓度计99", "Monitoring_value": "81%",
          "alarm": "是"},
         {"id": "130000197506257729", "time": "2015-03-26 14:56:35", "instrument": "浓度计19", "Monitoring_value": "79%",
          "alarm": "否"},
         {"id": "32000020081118233X", "time": "1977-08-18 23:55:14", "instrument": "浓度计16", "Monitoring_value": "7%",
          "alarm": "否"},
         {"id": "820000198709084774", "time": "1998-10-04 18:14:49", "instrument": "浓度计65", "Monitoring_value": "62%",
          "alarm": "否"},
         {"id": "32000019700619806X", "time": "2011-03-30 20:34:45", "instrument": "浓度计55", "Monitoring_value": "65%",
          "alarm": "是"},
         {"id": "310000200710178399", "time": "2013-07-24 16:41:16", "instrument": "浓度计81", "Monitoring_value": "16%",
          "alarm": "是"},
         {"id": "140000201212142310", "time": "2014-08-17 01:03:39", "instrument": "浓度计70", "Monitoring_value": "53%",
          "alarm": "是"},
         {"id": "520000200810243062", "time": "1995-08-30 16:07:15", "instrument": "浓度计71", "Monitoring_value": "93%",
          "alarm": "是"},
         {"id": "150000198712032289", "time": "1983-12-19 20:59:57", "instrument": "浓度计65", "Monitoring_value": "22%",
          "alarm": "否"},
         {"id": "150000199004104223", "time": "2001-05-25 12:57:07", "instrument": "浓度计97", "Monitoring_value": "42%",
          "alarm": "否"},
         {"id": "360000200303191990", "time": "1993-05-20 13:40:04", "instrument": "浓度计30", "Monitoring_value": "87%",
          "alarm": "否"},
         {"id": "350000198511035026", "time": "1991-05-23 11:38:55", "instrument": "浓度计57", "Monitoring_value": "28%",
          "alarm": "否"},
         {"id": "810000198005035374", "time": "2001-06-24 09:49:39", "instrument": "浓度计71", "Monitoring_value": "87%",
          "alarm": "是"},
         {"id": "310000201808198333", "time": "2016-07-10 09:02:18", "instrument": "浓度计10", "Monitoring_value": "63%",
          "alarm": "否"},
         {"id": "450000198809268600", "time": "1972-06-20 09:16:38", "instrument": "浓度计64", "Monitoring_value": "24%",
          "alarm": "是"},
         {"id": "420000197905012592", "time": "2012-12-03 22:11:00", "instrument": "浓度计1", "Monitoring_value": "85%",
          "alarm": "否"},
         {"id": "220000198612317656", "time": "1971-10-21 10:35:48", "instrument": "浓度计33", "Monitoring_value": "98%",
          "alarm": "是"},
         {"id": "540000199909013812", "time": "2018-10-16 03:49:57", "instrument": "浓度计52", "Monitoring_value": "17%",
          "alarm": "是"},
         {"id": "650000198710078310", "time": "1978-03-25 22:31:40", "instrument": "浓度计77", "Monitoring_value": "69%",
          "alarm": "否"},
         {"id": "370000199501034992", "time": "2002-03-04 16:09:32", "instrument": "浓度计83", "Monitoring_value": "50%",
          "alarm": "是"},
         {"id": "810000201711133638", "time": "1976-12-07 16:47:28", "instrument": "浓度计73", "Monitoring_value": "17%",
          "alarm": "是"}]
    return jresponse(myFilter(l))


@app.route('/stope/stope_list', methods=['POST'])
def stope():
    l = [{"id": "330000197810038544", "create_time": "2001-04-02 08:55:01", "account": "admin9", "percentage": "88%",
          "status": "进行", "serial": 9},
         {"id": "210000197408030168", "create_time": "1984-06-06 02:53:00", "account": "admin2", "percentage": "34%",
          "status": "进行", "serial": 4},
         {"id": "220000197612316245", "create_time": "1980-03-15 10:20:38", "account": "admin7", "percentage": "66%",
          "status": "完成", "serial": 6},
         {"id": "54000019721126213X", "create_time": "1986-06-25 22:25:41", "account": "admin7", "percentage": "74%",
          "status": "完成", "serial": 8},
         {"id": "130000197206132027", "create_time": "2001-10-04 14:32:49", "account": "admin7", "percentage": "6%",
          "status": "进行", "serial": 5},
         {"id": "35000019901114233X", "create_time": "1997-07-05 17:43:24", "account": "admin7", "percentage": "55%",
          "status": "完成", "serial": 6},
         {"id": "320000200110277387", "create_time": "1978-01-18 22:05:15", "account": "admin7", "percentage": "77%",
          "status": "进行", "serial": 2},
         {"id": "340000199706211176", "create_time": "2017-07-19 09:45:04", "account": "admin4", "percentage": "62%",
          "status": "进行", "serial": 8},
         {"id": "210000199312036222", "create_time": "2000-10-16 08:03:34", "account": "admin5", "percentage": "89%",
          "status": "完成", "serial": 7},
         {"id": "530000198111117915", "create_time": "1973-12-01 20:50:27", "account": "admin7", "percentage": "32%",
          "status": "进行", "serial": 7},
         {"id": "610000199711157851", "create_time": "1996-11-13 12:21:34", "account": "admin2", "percentage": "75%",
          "status": "进行", "serial": 6},
         {"id": "53000020170620801X", "create_time": "1994-08-09 02:56:29", "account": "admin9", "percentage": "87%",
          "status": "进行", "serial": 8},
         {"id": "450000199205147407", "create_time": "1996-07-10 22:17:15", "account": "admin1", "percentage": "8%",
          "status": "完成", "serial": 4},
         {"id": "640000200502279327", "create_time": "1971-10-24 09:01:21", "account": "admin4", "percentage": "40%",
          "status": "进行", "serial": 3},
         {"id": "140000200302258187", "create_time": "1981-08-10 23:29:45", "account": "admin2", "percentage": "57%",
          "status": "完成", "serial": 5},
         {"id": "820000198103132225", "create_time": "1977-04-02 05:49:14", "account": "admin6", "percentage": "51%",
          "status": "进行", "serial": 5},
         {"id": "510000201602073786", "create_time": "2018-04-02 19:37:04", "account": "admin4", "percentage": "4%",
          "status": "完成", "serial": 1},
         {"id": "22000019700911557X", "create_time": "1973-05-28 10:01:34", "account": "admin8", "percentage": "54%",
          "status": "完成", "serial": 9},
         {"id": "150000198704202161", "create_time": "1991-05-05 21:54:44", "account": "admin9", "percentage": "55%",
          "status": "进行", "serial": 4},
         {"id": "82000020121216620X", "create_time": "2013-09-27 10:33:04", "account": "admin2", "percentage": "75%",
          "status": "进行", "serial": 9},
         {"id": "650000198103062763", "create_time": "1972-08-06 05:47:58", "account": "admin5", "percentage": "68%",
          "status": "完成", "serial": 2},
         {"id": "140000201310103542", "create_time": "1995-12-16 19:28:55", "account": "admin3", "percentage": "91%",
          "status": "完成", "serial": 10},
         {"id": "610000200411140885", "create_time": "1984-02-29 01:30:45", "account": "admin8", "percentage": "43%",
          "status": "进行", "serial": 6},
         {"id": "820000199309080680", "create_time": "2016-05-21 23:55:04", "account": "admin5", "percentage": "30%",
          "status": "进行", "serial": 7},
         {"id": "610000198510070114", "create_time": "1992-08-26 00:25:39", "account": "admin5", "percentage": "36%",
          "status": "进行", "serial": 2},
         {"id": "450000199507246494", "create_time": "1971-11-15 14:03:50", "account": "admin9", "percentage": "46%",
          "status": "进行", "serial": 6},
         {"id": "220000201609212532", "create_time": "1985-06-14 03:13:51", "account": "admin8", "percentage": "30%",
          "status": "完成", "serial": 4},
         {"id": "210000200212122217", "create_time": "1986-06-21 10:12:17", "account": "admin3", "percentage": "40%",
          "status": "完成", "serial": 6},
         {"id": "71000020151019748X", "create_time": "1988-11-08 15:57:59", "account": "admin6", "percentage": "12%",
          "status": "进行", "serial": 4},
         {"id": "810000198301055185", "create_time": "2001-01-12 05:49:36", "account": "admin1", "percentage": "51%",
          "status": "完成", "serial": 6}]
    return jresponse(myFilter(l))


@app.route('/dataset1/all', methods=['GET'])
def dataset_all():
    temp = []
    now = int(time.time())
    for i in range(60 * 24):
        temp.append([now * 1000, random.randint(0, 100)])
        now -= 60
    l = {
        'warning': [['Jan 03', 547, 202], ['Jan 04', 169, 248], ['Jan 05', 233, 508], ['Jan 06', 576, 394],
                    ['Jan 07', 81, 104], ['Jan 08', 325, 277], ['Jan 09', 790, 661], ['Jan 10', 353, 633],
                    ['Jan 11', 562, 406], ['Jan 12', 357, 607], ['Jan 13', 455, 369], ['Jan 14', 699, 784],
                    ['Jan 15', 546, 353], ['Jan 16', 261, 318], ['Jan 17', 658, 442], ['Jan 18', 468, 724],
                    ['Jan 19', 290, 207], ['Jan 20', 335, 144], ['Jan 21', 619, 536], ['Jan 22', 434, 486],
                    ['Jan 23', 307, 556], ['Jan 24', 523, 300], ['Jan 25', 334, 411], ['Jan 26', 748, 542],
                    ['Jan 27', 565, 164], ['Jan 28', 494, 513], ['Jan 29', 403, 799], ['Jan 30', 797, 691],
                    ['Jan 31', 772, 267], ['Feb 01', 547, 202], ['Jan 02', 169, 248]],
        'charts': [
            {
                'name': '进料浓度',
                'data': temp,
                'param': 'concentration'
            },
            {
                'name': '进料流量',
                'param': 'flow',
                'data': temp
            },
            {
                'name': '浓密机底流浓度',
                'param': 'concentration',
                'data': temp
            },
            {
                'name': '充填浓度',
                'param': 'concentration',
                'data': temp
            },
            {
                'name': '充填流量',
                'param': 'flow',
                'data': temp
            },
            {
                'name': '水泥流量',
                'param': 'flow',
                'data': temp
            }
        ]}
    return jresponse1(l)


@app.route('/dataset1/update', methods=['POST'])
def dataset_update():
    temp = []
    now = int(time.time())
    temp.append([now * 1000, random.randint(0, 100)])
    l = [
        {
            'name': '进料浓度',
            'data': temp,
        },
        {
            'name': '进料流量',
            'data': temp
        },
        {
            'name': '浓密机底流浓度',
            'data': temp
        },
        {
            'name': '充填浓度',
            'data': temp
        },
        {
            'name': '充填流量',
            'data': temp
        },
        {
            'name': '水泥流量',
            'data': temp
        }
    ]
    return jresponse1(l)


@app.route('/inst/detail/all', methods=['GET'])
def inst_detail():
    temp = []
    now = int(time.time())
    for i in range(24 * 60):
        temp.append([now * 1000, random.randint(0, 100)])
        now -= 60
    l = {'detail': [{ 'key': '仪器（编号 / 名称）', 'value': 'AT-311TH01 / 浊度计' }, 
        { 'key': '点位描述', 'value': '311TH01 - Tailings Thickener 1 Overflow Turbidity Transmitter' }, 
        { 'key': '正常工作最大值', 'value': '-5' }, { 'key': '正常工作最小值', 'value': '10' }, 
        { 'key': '最近数据采集时间', 'value': '2006-01-06 00:42:54' }, 
        { 'key': '点位隶属', 'value': '美卓 或 恩菲 METSO or ENFI' }, 
        { 'key': 'OPC tag', 'value': '对应数据库里的设备tag ：ins_tag' }],

         'charts': [
             {
                 'name': '浓度',
                 'data': temp,
                 'param': 'concentration'
             }
         ]}
    return jresponse1(l)


@app.route('/inst/detail/update', methods=['POST'])
def inst_detail_update():
    l = {
        'key': getPostData('key', ''),
        'value': getPostData('value', '')
    }
    return jresponse1(l)
@app.route('/inst/detail/updateChart', methods=['POST'])
def inst_detail_chart_update():
    temp = []
    now = int(time.time())
    temp.append([now * 1000, random.randint(0, 100)])
    l = [
        {
            'name': '浓度',
            'data': temp
        }
    ]
    return jresponse1(l)


@app.route('/paste/thickener/chart/all', methods=['GET'])
def thickener_chart_all():
    temp = []
    now = int(time.time())
    for i in range(24 * 60):
        temp.append([now * 1000, random.randint(0, 100)])
        now -= 60
    l = [
        {
            'name': '浊度曲线',
            'data': temp,
            'param': 'flow'
        },
        {
            'name': '底流浓度曲线',
            'data': temp,
            'param': 'flow'
        }
    ]
    return jresponse1(l)


@app.route('/paste/thickener/chart/update', methods=['POST'])
def thickener_chart_update():
    temp = []
    now = int(time.time())
    temp.append([now * 1000, random.randint(0, 100)])
    l = [
        {
            'name': '浊度曲线',
            'data': temp
        },
        {
            'name': '底流浓度曲线',
            'data': temp
        }
    ]
    return jresponse1(l)


@app.route('/paste/mixer/chart/all', methods=['GET'])
def mixer_chart_all():
    temp = []
    now = int(time.time())
    for i in range(24 * 60):
        temp.append([now * 1000, random.randint(0, 100)])
        now -= 60
    l = [
        {
            'name': '填充浓度曲线',
            'data': temp,
            'param': 'flow'
        },
        {
            'name': '填充流量曲线',
            'data': temp,
            'param': 'flow'
        }
    ]
    return jresponse1(l)


@app.route('/paste/mixer/chart/update', methods=['POST'])
def mixer_chart_update():
    temp = []
    now = int(time.time())
    temp.append([now * 1000, random.randint(0, 100)])
    l = [
        {
            'name': '填充浓度曲线',
            'data': temp
        },
        {
            'name': '填充流量曲线',
            'data': temp
        }
    ]
    return jresponse1(l)


@app.route('/paste/mixer/paras/update', methods=['GET'])
def mixer_paras_all():
    l = ['cement_sp', 'cement_ratio_sp', 'concentration_out', 'flow_out', 'cement_height', 'height_1', 'speed_1',
         'cement_dosage', 'cement_sum', 'height_2', 'speed_2', 'cement_ratio', 'final_concentration_out',
         'final_flow_out','evenness_value','evenness_status']
    res = {i: random.randint(0, 10) for i in l}
    return jresponse1(res)


@app.route('/paste/thickener/paras/update', methods=['GET'])
def thickener_paras_all():
    l = ['rake_speed_sp', 'flocculant_sp', 'mode_sp', 'flow_out_sp', 'dosage_sp', 'pressure_sp', 'flow_in',
         'concentration_in', 'flocculant', 'torque_1', 'torque_2', 'torque_3', 'torque_4', 'rake_speed', 'sand_height',
         'sand_height_correct', 'pressure', 'tur', 'dilution_thin', 'dilution_thick', 'concentration_out', 'flow_out',
         'mode']
    res = {i: random.randint(0, 10) for i in l}
    return jresponse1(res)


if __name__ == '__main__':
    socketio.run(app, '0.0.0.0', port=25001, debug=True, use_reloader=False)
