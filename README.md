# 项目二期

> 项目使用了vue-element-admin后台框架，目前项目内已包含 Element UI & axios & iconfont & permission control & lint & vue-socketio。

[线上地址，仅供预览](http://806.ddns.hunsh.net:9528/)

线上地址若无法访问可能是测试服务器宕了或者不小心关闭了服务，可以电联15980848732通知我。

目前版本为 `v4.0+` 基于 `vue-cli` 进行构建。

## Build Setup

项目根目录缺失.env文件，如果是第一次拉取本项目，请从/env-exaple/中将.env*复制到项目根目录，否则无法运行!

```bash

# 进入项目目录
cd project-path

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装以来，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev

# 部分数据获取，如实时数据更新、列表数据更新依赖测试服务器，如果无法访问也请电联。
```

浏览器访问 [http://localhost:9528](http://localhost:9528)

## 前后端对接

现在的数据有两种，一种是简单的数据，写在/mock里，主要是login部
分
另一种数据是动态的，用python-flask写了一个简易服务端，在/testServer里，可以自行翻阅。

数据交换都采用json格式，目前都是post，要改请在/src/api里修改

***为了数据格式管理的方便，发送给后端的数据是json，后端请自行解析，不是标准的post body***

返回数据尽量放在data以保证数据格式一致性。

成功请求数据格式如下
```json
{
    "code": 20000,
    "data": {
        "items": [],
        "total": 0
    }
}
```
失败请求数据格式如下
```json
{
    "code": 111111,
    "message": "错误原因"
}
```
***成功请求的code一定是20000（代码位于/src/utils/requests.js），如果不是即认定为请求错误，失败的状态码可以自定义***

***此外内置了两个code：50012: 其他端登录; 50014: 登录过期/未登录。***

## 国际化

- 内置了国际化，语言包在/src/lang，打开就知道怎么改了。

- 如果要新增，请参照代码文件模仿，或者查看[vue-i18n文档](https://kazupon.github.io/vue-i18n/)。

- 数据的格式化问题（比如邮件和仪器名称这种），前端是不可能实现的，所以在每次请求的param里加了lang，后端需要自己实现翻译。

## 注意事项
- 现在api的鉴权是用token，在/user/login接口返回（具体在/mock/user.js）。

- 如果要换成session认证，把/src/store/modules/user.js中set_token去掉即可。

- 标题在语言包-title中修改

- 系统的icon，替换/public/favicon.ico即可


## 发布之前
 
- 校对数据格式
- 如果有跨域请配置好CORS
- 将本项目根目录下.env.production文件内 VUE_APP_BASE_API 及 VUE_APP_WEBSOCKET 修改为线上服务器地址

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## 其它

```bash
# 预览发布环境效果
npm run preview

# 预览发布环境效果 + 静态资源分析
npm run preview -- --report

# 代码格式检查
npm run lint

# 代码格式检查并自动修复
npm run lint -- --fix
```

更多信息请参考 [vue-element-admin文档](https://panjiachen.github.io/vue-element-admin-site/zh/)


## Browsers support

Modern browsers and Internet Explorer 10+.

| [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt="IE / Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>IE / Edge | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png" alt="Firefox" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Firefox | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png" alt="Chrome" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Chrome | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_48x48.png" alt="Safari" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Safari |
| --------- | --------- | --------- | --------- |
| IE10, IE11, Edge| last 2 versions| last 2 versions| last 2 versions


## 相关项目

[vue-element-admin](https://github.com/PanJiaChen/vue-element-admin)


